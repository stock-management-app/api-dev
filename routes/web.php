<?php

use App\Http\Controllers\CommandeController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\SectionController;
use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('home');
});
Route::get('/dashboard', function () {
    return view('admin/dashboard');
});

Route::resource('sections', SectionController::class);
Route::resource('items', ItemController::class);
Route::resource('commandes', CommandeController::class);
