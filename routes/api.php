<?php

use App\Http\Controllers\ClientController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\CommandeSupplierController;
use App\Http\Controllers\DepotController;
use App\Http\Controllers\DetailCommandeController;
use App\Http\Controllers\DetailStockController;
use App\Http\Controllers\DetailVenteController;
use App\Http\Controllers\EmployeeController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\SectionController;
use App\Http\Controllers\SocietyController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\VenteController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('sections', [SectionController::class,'all']);
Route::get('allitems', [ItemController::class, 'all']);
// Route::post('items', [ItemController::class,'store']);

Route::resource('items', ItemController::class);
Route::resource('clients', ClientController::class);
Route::resource('stocks', StockController::class);
Route::resource('detailStocks', DetailStockController::class);
Route::resource('detailCommandes', DetailCommandeController::class);
Route::resource('detailventes', DetailVenteController::class);
Route::resource('suppliers', SupplierController::class);
Route::resource('commandes', CommandeSupplierController::class);
Route::resource('commandes', CommandeController::class);
Route::resource('ventes', VenteController::class);
Route::resource('payments', PaymentController::class);
Route::resource('sections', SectionController::class);
Route::resource('societe', SocietyController::class);
Route::resource('depots', DepotController::class);
Route::resource('vehicles', VehicleController::class);
Route::resource('expenses', ExpenseController::class);
Route::resource('employees', EmployeeController::class);

Route::get('employeelogin/{username}/{password}', [EmployeeController::class,'login'])->name('username')->name('password');
Route::post('detailVentesUpdate', [DetailVenteController::class, 'postDetailVenteUpdate']);
Route::post('allPurchasesFilter', [StockController::class, 'getAllPurchasesFilter']);
Route::post('allPaymentsFilter', [PaymentController::class, 'getAllPaymentsFilter']);
Route::post('venteDetail', [VenteController::class, 'getDetailVente']);
Route::get('ventesDetails', [VenteController::class, 'getAllVentesDetails']);
Route::post('allVentesFilter', [VenteController::class, 'getAllVentesFilter']);
Route::get('allPeriod/{du}/{au}', [SocietyController::class, 'getAllPeriodPrint'])->name('du')->name('au');
// Route::get('expenses', [VehicleExpenseController::class, 'getAllVehicleExpense']);
Route::get('clients/slug/{slug}', [ClientController::class, 'getClientSlug'])->name('slug');
Route::get('commadesDetails', [CommandeController::class, 'getAllCommandesDetails']);
Route::get('paymentsDetails', [PaymentController::class, 'allPaymentsClients']);
Route::get('sendmail', [SupplierController::class, 'sendMail']);
Route::post('store_file', [ItemController::class, 'fileStore']);
Route::get('itemsSection/{id}', [ItemController::class, 'itemsSection'])->name('id');
Route::get('paymentsClients/{slugClient}', [CommandeController::class, 'getPaymentsClients'])->name('slugClient');
