<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style>
    table,
    th,
    td {
      border: 1px solid black;
      border-collapse: collapse;
      padding-left: 10px;
    }

    table {
      width: 100%;
      border-collapse: collapse;
      border-radius: 6px;
    }

    thead {
      text-align: left;
      border-radius: 6px;
      height: 1cm;
      /* background-color: #f55252; */
    }

    thead tr th {
      border-color: black;
    }

    tbody tr {
      height: 1cm;
      padding-left: 10px;
    }

    .remarque {
      font-weight: bolder;
      font-size: 15px;
      color: red;
    }

    .flex {
      display: flex;
      width: 99.9%;
    }

    h2 {
      border-bottom: 2px solid black;
    }

    .m-0 {
      margin: 0;
    }

    .raduis-5px {
      border-radius: 5px;
    }
  </style>
</head>

<body>

  <div class="flex">
    <fieldset style="width: 49%;" class="m-0 raduis-5px">
      <legend>Fournisseur:</legend>
      <!-- <div> -->
      <div style="
                width: 100%;
                font-size: 14px;
              ">
      </div>
      <div style="
                text-align: left;
                font-size: 14px;
              ">
        {{ $supplier['fullName'] }}
      </div>
      <div style="
                width: 100%;
                font-size: 14px;
              ">
        {{ $supplier['address'] }}
      </div>

      <div style="
                width: 100%;
                font-size: 14px;
              ">
        Tel {{ $supplier['phone'] }}
        Email {{ $supplier['email'] }}
      </div>
      <!-- </div> -->
    </fieldset>

    <div style="width: 49%;">
      <fieldset style="width: 100%;" class="m-0">
        <legend>Adresse de Livraison :</legend>
        <div>
          <div style="
                width: 100%;
                font-size: 14px;
              ">
            {{ $commande['addressLaivraison'] }}
          </div>
        </div>
      </fieldset>
      <fieldset style="width: 100%;" class="m-0">
        <legend>Adresse de Facturation :</legend>
        <div>
          <div style="
                width: 100%;
                
                
                font-size: 14px;
              ">
            <!-- <img src="favicon1.png" style="min-width: 3cm;max-width:5cm; max-height: 2cm" /> -->
          </div>
          <div style="
                width: 100%;
                
                
                font-size: 14px;
              ">
            {{ $society['fullName'] }}
          </div>
          <div style="
                width: 100%;
                
                
                font-size: 14px;
              ">
            {{ $society['address'] }} ,
            {{ $society['ville'] }}
            {{ $society['codePostal'] }}
          </div>
          <div style="
                width: 100%;
                
                
                font-size: 14px;
              ">
            ICE : {{ $society['ice'] }} RC :
            {{ $society['rc'] }} IF :
            {{ $society['_if'] }} CNSS :
            {{ $society['cnss'] }}
          </div>
          <div style="
                width: 100%;
                
                
                font-size: 14px;
              ">
            Tel {{ $society['phoneA'] }} /
            {{ $society['phoneB'] }}
          </div>
        </div>
      </fieldset>
    </div>
  </div>
  <table>
    <thead>
      <tr>
        <th width="30%">Article</th>
        <th width="10%">Qte</th>
        <th width="60%">Remarque</th>
      </tr>
    </thead>
    <tbody>
      @foreach($commande_details as $plaza)
      <tr>
        <td width="30%">{{ $plaza['nameItem'] }}</td>
        <td width="10%">{{ $plaza['quantity'] }}</td>
        <td width="30%">{{ $plaza['remark'] }}</td>
      </tr>
    </tbody>
    @endforeach
  </table>

  @if($commande)
  <br>
  Reference Commande : {{ $commande['reference'] }}
  <br>
  @if($commande['remark'])
  <div class="remarque">
    Note : {{ $commande['remark'] }}
  </div>
  @endif
  @endif
</body>

</html>