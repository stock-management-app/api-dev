<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupplierMail extends Mailable
{
    use Queueable, SerializesModels;
    public  $title;
    public $supplier;
    public $commande_details;
    public $commande;
    public $society;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($title, $supplier_details, $commandes,$commande,$society)
    {
        //
        $this->title = $title;
        $this->supplier = $supplier_details;
        $this->commande_details = $commandes;
        $this->commande = $commande;
        $this->society = $society;
    }
    
    public function build()
    {
        return $this->subject($this->title)
            ->view('app');
    }
}
