<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use App\Models\Expense;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function index()
    {
        return Expense::all();
    }

    public function store(Request $request)
    {
        $vehicleExpense = new Expense();
        $vehicleExpense->referenceVehicle = $request->referenceVehicle;
        $vehicleExpense->depenses = $request->depenses;
        $vehicleExpense->amount = $request->amount;
        $vehicleExpense->fullNameUser  = $request->fullNameUser;

        $vehicleExpense->expenseDate = $request->expenseDate;
        $vehicleExpense->remark = $request->remark;

        $vehicleExpense->save();

        return Expense::where('referenceVehicle', '=', $vehicleExpense->referenceVehicle)->get();
    }

    public function show($reference)
    {
        return Expense::where('referenceVehicle', '=', $reference)->get();
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function getAllVehicleExpense()
    {
        $_section = Expense::where('referenceVehicle', '=', '1234567890')->get();
        foreach (Vehicle::all() as $vehicle) {
            $items = Expense::where('referenceVehicle', '=', '1234567890')->get();
            if (Expense::where('referenceVehicle', '=', $vehicle->reference)->get() != null) {

                $items->push($vehicle);

                foreach (Expense::where('referenceVehicle', '=', $vehicle->reference)->get() as $item) {
                    $items->push($item);
                }
                $_section->push(["vehicle" => $items]);
            }
        }
        return $_section;
    }
}
