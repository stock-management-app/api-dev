<?php

namespace App\Http\Controllers;

use App\Models\ItemSection;
use App\Models\Section;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    public function index()
    {
        return ItemSection::all();
        // return view('items.Index', compact('items'));
    }
    public function create()
    {
        return view('items.create');
    }
    public function store(Request $request)
    {
        $item = new ItemSection();
        $item->fullName = $request->fullName;
        $item->slug = Str::slug($request->fullName, '-');
        $item->grandComptePrice = $request->grandComptePrice;
        $item->specialPrice = $request->specialPrice;
        $item->particularPrice = $request->particularPrice;
        $item->professionalPrice = $request->professionalPrice;
        $item->tva = $request->tva;
        $item->remark = $request->remark;
        $item->section = $request->section;
        $item->slugSection = Str::slug($request->section, '-');
        $item->save();
        return ItemSection::all();
    }
    // function to store file in 'upload' folder
    public function fileStore(Request $request)
    {
        $item = new ItemSection();

        $upload_path = public_path('api/upload');
        $file_name = $request->file->getClientOriginalName();
        // $generated_new_name = $item->id . '.' . $request->file->getClientOriginalExtension();

        $item->fullName = $request->fullName;
        $item->save();
        $lastItem = ItemSection::find($item->id);
        $generated_new_name = $item->id . '.' . $request->file->getClientOriginalExtension();

        $lastItem->photo = 'upload/' . $generated_new_name;
        $lastItem->save();
        $request->file->move($upload_path, $generated_new_name);

        return response()->json(['success' => 'You have successfully uploaded']);
        // return response()->json($request->item);
    }

    public function show($id)
    {
        return ItemSection::findOrFail($id);
    }
    public function edit(ItemSection $item)
    {
    }

    public function update($id, Request $request)
    {

        $item = ItemSection::find($id);

        $item->fullName = $request->fullName;
        $item->slug = Str::slug($request->fullName, '-');
        $item->grandComptePrice = $request->grandComptePrice;
        $item->specialPrice = $request->specialPrice;
        $item->particularPrice = $request->particularPrice;
        $item->professionalPrice = $request->professionalPrice;
        $item->tva = $request->tva;
        $item->quantity = $request->quantity;
        $item->remark = $request->remark;
        $item->section = $request->section;
        $item->slugSection = Str::slug($request->section, '-');

        $item->save();
        return ItemSection::all();
    }
    public function destroy(ItemSection $item)
    {
    }

    // API`
    public function all()
    {
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        foreach (Section::select('fullName')->get() as $section) {
            $items = ItemSection::where('fullName', '=', '1234567890')->get();
            $items->push(["nameSection" => $section->fullName]);
            foreach (ItemSection::where('section', '=', $section->fullName)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["section" => $items]);
        }
        return $_section;
        // return Section::select('fullName')->get();
    }
    public function itemsSection($id)
    {
        return ItemSection::where('idSection', '=', $id)->get();
    }
}
