<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class EmployeeController extends Controller
{

    public function index()
    {
        return Employee::all();
    }
    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $employee = new Employee();
        $employee->firstName = Str::ucfirst($request->firstName);
        $employee->lastName =  strtoupper($request->lastName);
        $employee->depot = $request->depot;
        $employee->phone = $request->phone;
        $employee->username = $request->username;
        $employee->fullName = $employee->lastName . ' ' . $employee->firstName;
        $employee->password = $request->password;
        $employee->remark = $request->remark;

        $employee->save();
    }

    public function show($username, $password)
    {
        return Employee::where('username', '=', $username)->where('password', '=', $password)->first();
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
    public function login($username, $password)
    {
        // return [$username , $password];
        return Employee::where('username', '=', $username)->where('password', '=', $password)->select('fullName', 'username')->first();
    }
}
