<?php

namespace App\Http\Controllers;

use App\Mail\SupplierMail;
use App\Models\Society;
use App\Models\Supplier;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SupplierController extends Controller
{
    public function index()
    {
        return Supplier::all();
        // return view('items.Index', compact('items'));
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
        $lastSupplier = Supplier::latest()->first();

        if (!$lastSupplier) {
            $reference = 'F000000';
        } else if ($lastSupplier != null & $lastSupplier->reference == null) {
            $reference = 'F000000';
        } else {
            $reference = $lastSupplier->reference;
        }
        $reference++;
        $item = new Supplier();
        $item->fullName = $request->firstName . " " . $request->lastName;
        $item->slug = Str::slug($item->fullName, '-');
        $item->firstName = $request->firstName;
        $item->lastName = $request->lastName;
        $item->phone = $request->phone;
        $item->email = $request->email;
        $item->address = $request->address;
        $item->ville = $request->ville;
        $item->codePostal = $request->codePostal;
        $item->remark = $request->remark;
        $item->reference = $reference;
        $item->save();
        return Supplier::all();
    }


    public function show($id)
    {
        return Supplier::findOrFail($id);
    }
    public function edit(Supplier $item)
    {
    }

    public function update($id, Request $request)
    {
        $item = Supplier::find($id);
        $item->fullName = $request->firstName . " " . $request->lastName;
        $item->slug = Str::slug($item->fullName, '-');
        $item->firstName = $request->firstName;
        $item->lastName = $request->lastName;
        $item->phone = $request->phone;
        $item->email = $request->email;
        $item->address = $request->address;
        $item->ville = $request->ville;
        $item->codePostal = $request->codePostal;
        $item->remark = $request->remark;
        $item->save();

        return Supplier::all();
    }
    public function destroy(Supplier $item)
    {
    }

    // API
    public function all()
    {
        return Supplier::all();
    }
    public function itemsSection($id)
    {
        return Supplier::where('idSection', '=', $id)->get();
    }

    public function sendMail()
    {
        $society = ['nom'=>'Good Groupe'];
        $title = '[Confirmation] Thank you for your order';
        $customer_details = [
            'name' => 'Arogya',
            'address' => 'kathmandu Nepal',
            'phone' => '123123123',
            'email' => 'nom18022021@gmail.com'
        ];
        $order_details = [
            'SKU' => 'D-123456',
            'price' => '10000',
            'order_date' => '2020-01-22',
        ];
        $commande = [
            'id'=>'123'
        ];
        $sendmail = Mail::to($customer_details['email'])->send(new SupplierMail($title, $customer_details, $order_details, $commande, $society));
        if (empty($sendmail)) {
            return response()->json(['message' => 'Mail Sent Sucssfully'], 200);
        } else {
            return response()->json(['message' => 'Mail Sent fail'], 400);
        }
    }
}
