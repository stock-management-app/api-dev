<?php

namespace App\Http\Controllers;

use App\Models\Client;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    public function index()
    {
        return Client::all();
    }
    public function create()
    {
    }
    public function store(Request $request)
    {
        $lastClient = Client::latest()->first();

        if (!$lastClient) {
            $reference = 'CL000000';
        } else {
            $reference = $lastClient->reference;
        }
        $reference++;
        $client = new Client();
        $client->lastName = strtoupper($request->lastName);
        $client->firstName = Str::ucfirst($request->firstName);
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->password = $request->password;
        $client->maxCredit = $request->maxCredit;
        $client->typePurchase = $request->typePurchase;
        $client->remark = $request->remark;
        $client->society = $request->society;
        $client->rib = $request->rib;
        $client->responsable = $request->responsable;
        $client->address = $request->address;
        $client->ville = $request->ville;
        $client->codePostal = $request->codePostal;
        
        $client->fullName = $client->lastName ." " . $client->firstName;
        $client->slugClient =  Str::slug($client->fullName, '-');
        $client->reference = $reference;
        $client->save();
        return Client::all();
    }

    public function show($id)
    {
        return Client::findOrFail(intval($id));
    }
    public function getClientSlug($slug)
    {
        return Client::where('slugClient','=',$slug)->first();
    }

    public function update($id, Request $request)
    {
        $client = Client::find($id);
        // $client = new Client();
        $client->lastName = $request->lastName;
        $client->firstName = $request->firstName;
        $client->email = $request->email;
        $client->phone = $request->phone;
        $client->password = $request->password;
        $client->society = $request->society;
        $client->maxCredit = $request->maxCredit;
        $client->typePurchase = $request->typePurchase;
        $client->remark = $request->remark;
        $client->rib = $request->rib;
        $client->responsable = $request->responsable;
        $client->address = $request->address;
        $client->ville = $request->ville;
        $client->codePostal = $request->codePostal;
        
        $client->fullName = $request->lastName ." " . $request->firstName;
        $client->slugClient =  Str::slug($client->fullName, '-');
        $client->save();
        return Client::all();
    }
    public function destroy($id)
    {
        $client = Client::where('id','=',$id)->first();
        $client->delete();
        return Client::all();
    }
}
