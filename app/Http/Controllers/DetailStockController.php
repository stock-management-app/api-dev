<?php

namespace App\Http\Controllers;

use App\Models\DetailStock;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DetailStockController extends Controller
{
    public function index()
    {
        return DetailStock::where('idStock', '=', null)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $detailStock = new DetailStock();
        $detailStock->nameItem = $request->nameItem;
        $detailStock->fullNameUser  = $request->fullNameUser;

        $detailStock->quantity = $request->quantity;
        $detailStock->purchasePrice = $request->purchasePrice;
        $detailStock->totalPrice = $request->purchasePrice * $request->quantity;
        $detailStock->grandComptePrice  = $request->grandComptePrice;
        $detailStock->specialPrice  = $request->specialPrice;
        $detailStock->particularPrice  = $request->particularPrice;
        $detailStock->professionalPrice  = $request->professionalPrice;
        $detailStock->slugItem = Str::slug($request->nameItem, '-');
        // $detailStock->expirationDate = $request->expirationDate;
        // $detailStock->idUser = $request->idUser;

        $detailStock->save();

        return DetailStock::where('idStock', '=', null)->get();
    }
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        $item = DetailStock::where('id', '=', $id)->first();
        $item->delete();

        return DetailStock::where('idStock', '=', null)->get();
    }
}
