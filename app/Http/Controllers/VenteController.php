<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Vente;
use App\Models\DetailVente;
use App\Models\ItemSection;
use App\Models\Payment;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class VenteController extends Controller
{
    public function index()
    {
        $totalPriceVenteToday = 0;
        $totalPayrollAmount = 0;
        $totalPayrollAmountCheque = 0;
        $totalPayrollAmountEspace = 0;
        $totalLeftAmount = 0;
        $today = date('Y-m-d');

        foreach (Vente::where('dateVente', '=', $today)
            ->get() as $Vente) {

            $totalPriceVenteToday = $Vente->totalPriceTTC + $totalPriceVenteToday;
            $totalPayrollAmount = $Vente->payrollAmount + $totalPayrollAmount;
            $totalLeftAmount = $Vente->leftAmount + $totalLeftAmount;
            // if ($Vente->paymentMethod == 'Chèque' & $Vente->etatPayment == 'Oui') {

            //     $totalPayrollAmountCheque = $Vente->payrollAmount + $totalPayrollAmountCheque;
            // }
            // if ($Vente->paymentMethod == 'Espèces' & $Vente->etatPayment == 'Oui') {

            //     $totalPayrollAmountEspace = $Vente->payrollAmount + $totalPayrollAmountEspace;
            // }
        }
        foreach (Payment::where('datePayment', '=', $today)
            ->get() as $payment) {

            if ($payment->paymentMethod == 'Chèque') {

                $totalPayrollAmountCheque = $payment->amount + $totalPayrollAmountCheque;
            }
            if ($payment->paymentMethod == 'Espèces') {

                $totalPayrollAmountEspace = $payment->amount + $totalPayrollAmountEspace;
            }
        }

        return [
            ["name" => "Montant Total des Ventes d'aujourd'hui", "value" => $totalPriceVenteToday],
            ["name" => "Montant Total Payé", "value" => $totalPayrollAmount],
            ["name" => "Montant Total Impayé", "value" => $totalLeftAmount],
            ["name" => "Montant Total Payé par Chèques", "value" => $totalPayrollAmountCheque],
            ["name" => "Montant Total Payé en Espèces", "value" => $totalPayrollAmountEspace],
        ];
    }

    public function store(Request $request)
    {
        // select client 
        $client = Client::where('slugClient', '=', Str::slug($request->nameClient, '-'))->first();

        $lastVente = Vente::latest()->first();

        if (!$lastVente) {
            $billNumber = 'BL000000';
        } else {
            $billNumber = $lastVente->billNumber;
        }
        $billNumber++;

        $Vente = new Vente();
        // $Vente->idClient  = $client->id;
        $Vente->nameClient  = $request->nameClient;
        $Vente->fullNameUser  = $request->fullNameUser;

        $Vente->dateVente  = $request->dateVente;

        $Vente->billNumber  = $billNumber;
        $Vente->slugClient  = Str::slug($request->nameClient, '-');
        $Vente->stock  = $request->stock;
        $Vente->dateDelivrance  = new DateTime();
        $Vente->save();


        $_totalPriceHT = 0;
        $_totalPriceTTC = 0;
        $_totalTVA = 0;
        $priceHT = 0;
        foreach (DetailVente::where('idCommande', '=', null)->get() as $req) {

            $item = ItemSection::where('slug', '=', $req->slugItem)->first();
            $tva = $item->tva;

            $item->quantity = $item->quantity - $req->quantity;
            // if ($request->stock == 'oui') {
            // }
            $item->save();

            // price HT (type client)
            if ($client->typePurchase == "Grand Compte") {
                $priceHT = $item->grandComptePrice;
            }
            if ($client->typePurchase == "Spéciale") {
                $priceHT = $item->specialPrice;
            }
            if ($client->typePurchase == "Particulier") {
                $priceHT = $item->particularPrice;
            }
            if ($client->typePurchase == "Professionnel") {
                $priceHT = $item->professionalPrice;
            }
            // Tva Item (produit)
            //Calcule total price HT
            $totalPriceHT = $priceHT * $req->quantity;
            // Calcul TTC
            $priceTTC = $priceHT + ($priceHT / 100) * $tva;
            //Calcul total TTC
            $totalPriceTTC = $priceTTC * $req->quantity;
            //Calcul total TVA
            $totalTVA = ($priceHT / 100) * $tva * $req->quantity;

            $req->idCommande = $Vente->id;
            $req->priceHT = $priceHT;
            // $req->priceHT = 123;
            $req->totalPriceHT = $totalPriceHT;
            $req->priceTTC = $priceTTC;
            $req->totalPriceTTC = $totalPriceTTC;
            $req->totalTVA = $totalTVA;
            $req->tva = $tva;
            $req->save();

            $_totalPriceHT = $_totalPriceHT + $totalPriceHT;
            $_totalPriceTTC = $_totalPriceTTC + $totalPriceTTC;
            $_totalTVA = $_totalTVA + $totalTVA;
        }

        $Vente->totalPriceHT  = $_totalPriceHT;
        $Vente->totalPriceTTC  = $_totalPriceTTC;
        $Vente->totalTVA  = $_totalTVA;
        if ($request->etatPayment == 'Oui') {
            $Vente->payrollAmount = $request->payrollAmount;
            $Vente->leftAmount  = $_totalPriceTTC - $request->payrollAmount;
            $Vente->paymentMethod = $request->paymentMethod;
            $Vente->etatPayment = 'Oui';

            // add to payments table
            $payment = new Payment();
            $payment->amount = $request->payrollAmount;
            $payment->nameClient = $request->nameClient;
            $payment->datePayment = $request->dateVente;
            $payment->paymentMethod = $request->paymentMethod;
            if ($payment->paymentMethod == 'Chèque') {
                $payment->cheque = 'Oui';
                $payment->chequeNumber = $request->chequeNumber;
            }
            $payment->slugClient = Str::slug($request->nameClient, '-');
            $payment->save();
        } else if ($request->etatPayment == 'Non') {
            $Vente->leftAmount  = $_totalPriceTTC;
        }
        $Vente->save();

        return $Vente;
    }
    public function show(Vente $Vente)
    {

        $items = DB::table('items')

            ->join('detail_Ventes', 'items.id', '=', 'detail_Ventes.idItem')

            ->join('Ventes', 'detail_Ventes.idCommande', '=', 'Ventes.id')

            ->join('clients', 'Ventes.idClient', '=', 'clients.id')

            ->select('items.*', 'detail_Ventes.idCommande')
            ->where('Ventes.id', '=', $Vente->id)
            ->where('etat', '=', 'none')
            ->get();
        return view('Ventes.show', compact('items'));
    }
    public function edit(Vente $Vente)
    {
    }
    public function update($id, Request $request)
    {
        $vente = Vente::find($id);
        $vente->nameClient  = $request->nameClient;

        $vente->dateVente  = $request->dateVente;

        $vente->slugClient  = Str::slug($request->nameClient, '-');
        $vente->stock  = $request->stock;
        $vente->dateDelivrance  = new DateTime();

        $_totalPriceHT = 0;
        $_totalPriceTTC = 0;
        $_totalTVA = 0;
        $priceHT = 0;
        foreach (DetailVente::where('idCommande', '=', $id)->get() as $req) {
            // Tva Item (produit)
            //Calcule total price HT
            $_totalPriceHT = $_totalPriceHT + $req->totalPriceHT;
            //Calcul total TTC
            $_totalPriceTTC = $_totalPriceTTC * $req->totalPriceTTC;
            //Calcul total TVA
            $_totalTVA = $_totalTVA + $req->totalTVA;
        }
        $vente->totalPriceHT  = $_totalPriceHT;
        $vente->totalPriceTTC  = $_totalPriceTTC;
        $vente->totalTVA  = $_totalTVA;
    }
    public function destroy(Vente $Vente)
    {
    }
    // api
    public function getAllVentesDetails()
    {
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        foreach (Vente::all() as $Vente) {
            $items = ItemSection::where('fullName', '=', '1234567890')->get();
            $items->push($Vente);
            foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["vente" => $items]);
        }
        return $_section;

        // return Vente::all();
    }

    public function getPaymentsClients($slugClient)
    {

        $dataReturn = DB::table('clients')->where('id', '=', '0')->get();

        $client = DB::table('clients')->where('clients.slugClient', '=', $slugClient)->get();

        // $Ventes=DB::table('Ventes')->where('Ventes.slugClient', '=', $client[0]->slugClient)->get();
        $Ventes = DB::table('Ventes')->where('Ventes.slugClient', '=', "1-2")->get();
        $cmd = DB::table('Ventes')->where('Ventes.slugClient', '=', "1-2")->get();

        // $dataReturn->push(["client"=>$client,"Ventes"=>[$Ventes,"detailsVentes"=>12]]);

        // $dataReturn = $client;
        foreach (DB::table('Ventes')->where('Ventes.slugClient', '=', $client[0]->slugClient)->get() as $Vente) {
            $Ventes->push([
                "Vente" => $Vente,
                "detailVentes" => DB::table('detail_Ventes')->where('detail_Ventes.idCommande', '=', $Vente->id)->get()
            ]);

            // foreach (DB::table('detail_Ventes')->where('detail_Ventes.idCommande', '=', $Vente->id)->get() as $DetailVente) {
            //     $dataReturn->push(["detailVente"=>$DetailVente]);
            // }
        }
        $client->push(["Ventes" => $Ventes]);
        // $dataReturn->push($Ventes);
        $dataReturn = $client;


        return $dataReturn;
    }
    public function getAllVentesFilter(Request $request)
    {
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        $type = "";
        if ($request) {
            // du au type
            if ($request->du != '' & $request->au != '' & $request->type != '') {
                foreach (Vente::where('dateVente', '>=', $request->du)
                    ->where('dateVente', '<=', $request->au)
                    ->get() as $Vente) {

                    $client = Client::where('slugClient', '=', $Vente->slugClient)->first();
                    if ($client->typePurchase == $request->type) {
                        // $type = $client->typePurchase;
                        $items = ItemSection::where('fullName', '=', '1234567890')->get();
                        $items->push($Vente);
                        foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                            $items->push($item);
                        }
                        $_section->push(["vente" => $items]);
                    }
                }
                // return [$_section,$type];
                return $_section;
                // return $type;
            }
            // du au
            if ($request->du != '' & $request->au != '' & $request->type == '' & $request->nameClient == '') {
                foreach (Vente::where('dateVente', '>=', $request->du)
                    ->where('dateVente', '<=', $request->au)
                    ->get() as $Vente) {
                    $items = ItemSection::where('fullName', '=', '1234567890')->get();
                    $items->push($Vente);
                    foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                        $items->push($item);
                    }
                    $_section->push(["vente" => $items]);
                }
                return $_section;
            }
            // type
            if ($request->type != '' & $request->du == '' & $request->au == '' & $request->nameClient == '') {
                foreach (Vente::all() as $Vente) {
                    $client = Client::where('slugClient', '=', $Vente->slugClient)->first();
                    if ($client->typePurchase == $request->type) {

                        $items = ItemSection::where('fullName', '=', '1234567890')->get();
                        $items->push($Vente);
                        foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                            $items->push($item);
                        }
                        $_section->push(["vente" => $items]);
                    }
                }
                return $_section;
            }
            // nameClient
            if ($request->nameClient != '' & $request->du == '' & $request->au == '' & $request->type == '') {
                $client = Client::where('fullName', '=', $request->nameClient)->first();
                $_section->push(["client"=>$client]);
                $ventes = ItemSection::where('fullName', '=', '1234567890')->get();
                foreach (Vente::where('nameClient', '=', $request->nameClient)->get() as $Vente) {
                    

                    $items = ItemSection::where('fullName', '=', '1234567890')->get();
                    $items->push($Vente);
                    foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                        $items->push($item);
                    }
                    $ventes->push(["vente" => $items]);
                }
                $_section->push(["ventes" => $ventes]);
                return $_section;
            }
            // nameclient du au 
            if ($request->nameClient != '' & $request->du != '' & $request->au != '' & $request->type == '') {
                foreach (Vente::where('nameClient', '=', $request->nameClient)
                    ->where('dateVente', '>=', $request->du)
                    ->where('dateVente', '<=', $request->au)->get() as $Vente) {

                    $items = ItemSection::where('fullName', '=', '1234567890')->get();
                    $items->push($Vente);
                    foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
                        $items->push($item);
                    }
                    $_section->push(["vente" => $items]);
                }
                return $_section;
            }
        } else {
            return null;
        }

        // return $_section;

        // return Vente::all();
    }

    public function getDetailVente(Request $request)
    {
        // select client 
        $client = Client::where('slugClient', '=', Str::slug($request->nameClient, '-'))->first();

        $lastVente = Vente::latest()->first();

        if (!$lastVente) {
            $billNumber = 'BL000000';
        } else {
            $billNumber = $lastVente->billNumber;
        }
        $billNumber++;

        $Vente = new Vente();
        // $Vente = new Vente();
        $Vente->idClient  = 1;
        $Vente->nameClient  = $request->nameClient;

        $Vente->dateVente  = $request->dateVente;

        $Vente->billNumber  = $billNumber;

        $priceHT = 0;
        $_totalPriceHT = 0;
        $_totalPriceTTC = 0;
        $_totalTVA = 0;

        $_detailVente = DetailVente::where('id', '=', '0')->get();
        foreach (DetailVente::where('idCommande', '=', null)->get() as $req) {

            $item = ItemSection::where('slug', '=', $req->slugItem)->first();
            $tva = $item->tva;

            // price HT (type client)
            if ($client->typePurchase == "Grand Compte") {
                $priceHT = $item->grandComptePrice;
            }
            if ($client->typePurchase == "Spéciale") {
                $priceHT = $item->specialPrice;
            }
            if ($client->typePurchase == "Particulier") {
                $priceHT = $item->particularPrice;
            }
            if ($client->typePurchase == "Professionnel") {
                $priceHT = $item->professionalPrice;
            }
            // Tva Item (produit)
            //Calcule total price HT
            $totalPriceHT = $priceHT * $req->quantity;
            // Calcul TTC
            $priceTTC = $priceHT + ($priceHT / 100) * $tva;
            //Calcul total TTC
            $totalPriceTTC = number_format($priceTTC * $req->quantity, 2, '.', '');
            //Calcul total TVA
            $totalTVA =  number_format(($priceHT / 100) * $tva * $req->quantity, 2, '.', '');

            $req->priceHT = $priceHT;
            $req->totalPriceHT = $totalPriceHT;
            $req->priceTTC = $priceTTC;
            $req->totalPriceTTC = $totalPriceTTC;
            $req->totalTVA = $totalTVA;
            $req->tva = $tva;
            $_detailVente->push($req);

            $_totalPriceHT = $_totalPriceHT + $totalPriceHT;
            $_totalPriceTTC = $_totalPriceTTC + $totalPriceTTC;
            $_totalTVA = $_totalTVA + $totalTVA;
        }

        $Vente->totalPriceHT  = $_totalPriceHT;
        $Vente->totalPriceTTC  = $_totalPriceTTC;
        $Vente->totalTVA  = $_totalTVA;

        $Vente->leftAmount  = $_totalPriceTTC;

        return [$Vente, $_detailVente];
    }
}
