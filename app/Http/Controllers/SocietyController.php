<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Commande;
use App\Models\DetailCommande;
use App\Models\DetailVente;
use App\Models\Expense;
use App\Models\ItemSection;
use App\Models\Payment;
use App\Models\Society;
use App\Models\Supplier;
use App\Models\Vehicle;
use App\Models\VehicleExpense;
use App\Models\Vente;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SocietyController extends Controller
{

    public function index()
    {
        return Society::all();
    }

    public function getAllPeriodPrint($du, $au)
    {
        // Expenses Vehicle
        $_expensesVehicle = Expense::where('referenceVehicle', '=', '1234567890')->get();
        foreach (Vehicle::all() as $vehicle) {
            $expenseVehicle = Expense::where('referenceVehicle', '=', '1234567890')->get();
            if (
                Expense::where('referenceVehicle', '=', $vehicle->reference)
                ->where('expenseDate', '>=', $du)
                ->where('expenseDate', '<=', $au)->first() != null
            ) {

                $expenseVehicle->push($vehicle);

                foreach (Expense::where('referenceVehicle', '=', $vehicle->reference)
                    ->where('expenseDate', '>=', $du)
                    ->where('expenseDate', '<=', $au)
                    ->get() as $item) {
                    $expenseVehicle->push($item);
                }
                $_expensesVehicle->push(["vehicle" => $expenseVehicle]);
            }
        }

        // detail Commandes
        $detailsVentes = ItemSection::where('fullName', '=', '1234567890')->get();
        foreach (Vente::where('dateVente', '>=', $du)
            ->where('dateVente', '<=', $au)->get() as $commande) {
            $items = ItemSection::where('fullName', '=', '1234567890')->get();
            $supplier = Client::where('slugClient', '=', $commande->slugClient)->first();
            $items->push($commande);
            $items->push($supplier);
            foreach (DetailVente::where('idCommande', '=', $commande->id)->get() as $item) {
                $items->push($item);
            }
            $detailsVentes->push(["vente" => $items]);
        }

        // paymmets Clients
        $paymmetsClients = Vente::where('nameClient', '=', '1234567890')->get();
        foreach (Vente::groupBy('nameClient')->select('nameClient')->get() as $vente) {

            $payments = Payment::where('nameClient', '=', '1234567890')->get();

            $client = Client::where('slugClient', '=', Str::slug($vente->nameClient))->first();
            if (Payment::where('slugClient', '=', $client->slugClient)
                ->where('datePayment', '>=', $du)
                ->where('datePayment', '<=', $au)->first() != null
            ) {

                $payments->push($client);

                foreach (Payment::where('slugClient', '=', $client->slugClient)
                    ->where('datePayment', '>=', $du)
                    ->where('datePayment', '<=', $au)->get() as $payment) {
                    $payments->push($payment);
                }
                $paymmetsClients->push(["payment" => $payments]);
            }
        }

        return [$detailsVentes, $paymmetsClients, $_expensesVehicle];
        // return [$detailsVentes];
        // return [$paymmetsClients];
        // return [$_expensesVehicle];
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update($id, Request $request)
    {
        $society = Society::find($request->id);

        $society->fullName = $request->fullName;
        $society->capital = $request->capital;
        $society->ville = $request->ville;
        $society->codePostal = $request->codePostal;
        $society->phoneA = $request->phoneA;
        $society->phoneB = $request->phoneB;
        $society->fax = $request->fax;
        $society->cnss = $request->cnss;
        $society->ice = $request->ice;
        $society->rc = $request->rc;
        $society->_if = $request->_if;
        $society->tp = $request->tp;
        $society->address = $request->address;
        $society->remark = $request->remark;

        $society->save();
        return Society::all();
        // return $society;
        // return $request;
    }

    public function destroy($id)
    {
        //
    }
}
