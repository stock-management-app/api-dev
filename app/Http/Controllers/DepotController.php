<?php

namespace App\Http\Controllers;

use App\Models\Depots;
use Illuminate\Http\Request;

class DepotController extends Controller
{
    
    public function index()
    {
        return Depots::all();
    }

    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $lastDepot = Depots::all()->last();

        if (!$lastDepot) {
            $reference = 'DP000000';
        } else {
            $reference = $lastDepot->reference;
        }
        $reference++;

        $depot = new Depots();
        $depot->fullName = $request->fullName;
        $depot->responsable = $request->responsable;
        $depot->phoneResponsable = $request->phoneResponsable;
        $depot->address = $request->address;
        $depot->ville = $request->ville;
        $depot->codePostal = $request->codePostal;
        $depot->reference = $reference;
        
        $depot->save();

        return Depots::all();
    }

    
    public function show($id)
    {
        return Depots::where('id','=',$id)->first();
    }

    public function edit($id)
    {
        //
    }

    public function update($id,Request $request)
    {
        // $depot = Depots::where('id','=',$request->id)->first();

        // $depot = new Depots();
        // $depot->fullName = $request->fullName;
        // $depot->responsable = $request->responsable;
        // $depot->phoneResponsable = $request->phoneResponsable;
        // $depot->address = $request->address;
        // $depot->ville = $request->ville;
        // $depot->codePostal = $request->codePostal;
        
        // $depot->save();

        // return Depots::all();
        return $request;
    }

    public function destroy($id)
    {
        //
    }
}
