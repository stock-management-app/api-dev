<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{

    public function index()
    {
        return Vehicle::all();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $lastVehicle = Vehicle::latest()->first();

        if (!$lastVehicle) {
            $reference = 'VL000000';
        } else if (!$lastVehicle->reference) {
            $reference = 'VL000000';
        } else {
            $reference = $lastVehicle->reference;
        }
        $reference++;

        $vehicle = new Vehicle();
        $vehicle->fullName = $request->fullName;
        $vehicle->matricule = $request->matricule;
        $vehicle->depot = $request->depot;
        $vehicle->remark = $request->remark;
        $vehicle->reference = $reference;
        $vehicle->save();
        return $vehicle;
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
