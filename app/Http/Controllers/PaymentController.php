<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Vente;
use App\Models\Payment;
use DateTime;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class PaymentController extends Controller
{
    public function index()
    {
        return Payment::all();
    }

    public function create()
    {
        //
    }

    public function credit($slugClient)
    {
        $_totalLeftAmount = 0;
        foreach (Vente::where('slugClient', '=', $slugClient)
            ->get() as $Vente) {

            $_totalLeftAmount = $Vente->leftAmount + $_totalLeftAmount;
        }
        $client = Client::where('slugClient', '=', $slugClient)->first();
        $client->credit = $_totalLeftAmount;
        $client->save();
    }
    public function store(Request $request)
    {
        $payment = new Payment();
        $payment->amount = $request->amount;
        $payment->nameClient = $request->nameClient;
        $payment->datePayment = $request->datePayment;
        $payment->paymentMethod = $request->paymentMethod;

        if ($payment->paymentMethod == 'Chèque') {
            $payment->cheque = 'Oui';
            $payment->chequeNumber = $request->chequeNumber;
            if ($request->chequePaid == 'Non') {
                $payment->chequePaid = 'Non';
                $payment->dateChequePaid = $request->dateChequePaid;
            } else if ($request->chequePaid == 'Oui') {
                $payment->chequePaid = 'Oui';
                $payment->dateChequePaid = new DateTime();
            }
        } else if ($payment->paymentMethod != 'Chèque') {
            $payment->cheque = 'Non';
        }
        $payment->slugClient = Str::slug($request->nameClient, '-');


        $payment->save();
        $payrollAmount = $payment->amount;
        // $VentesClient = Vente::where('slugClient', '=', $payment->slugClient)->get();

        foreach (Vente::where('slugClient', '=', $payment->slugClient)
            ->where('leftAmount', '>', 0)
            ->get() as $cmd) {

            if ($payrollAmount >= $cmd->leftAmount) {

                $cmd->payrollAmount = $cmd->leftAmount + $cmd->payrollAmount;
                $cmd->etatPayment = 'Oui';
                $cmd->paymentMethod =  $request->paymentMethod;
                $cmd->leftAmount = 0;
                $payrollAmount = $payrollAmount - $cmd->payrollAmount;
                $cmd->save();
            } else if ($payrollAmount < $cmd->leftAmount) {
                $cmd->etatPayment = 'Oui';
                $cmd->payrollAmount = $payrollAmount +  $cmd->payrollAmount;
                $cmd->leftAmount = $cmd->leftAmount - $payrollAmount;
                $cmd->paymentMethod =  $request->paymentMethod;
                // $payment->leftAmount  = $cmd->leftAmount;
                $payrollAmount = 0;
                $cmd->save();
            }
        }
        $total = 0;
        foreach (Vente::where('slugClient', '=', $payment->slugClient)
            ->where('leftAmount', '>', 0)->get() as $cmd) {
            $total = $total + $cmd->leftAmount;
        }

        $payment->leftAmount  = $total;
        $payment->save();
        //calcule credit

        /***********************************/
        // $_totalLeftAmount = 0;
        // foreach (Vente::where('slugClient', '=', Str::slug($request->nameClient, '-'))
        //     ->get() as $Vente) {

        //     $_totalLeftAmount = $Vente->leftAmount + $_totalLeftAmount;
        // }
        // $client = Client::where('slugClient', '=', Str::slug($request->nameClient, '-'))->first();
        // $client->credit = $_totalLeftAmount;
        // $client->save();
        /********************************* */
        // return Payment::where('idCommande', '=', null)->get();
        return Payment::all();
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function allPaymentsClients()
    {


        $_section = Vente::where('nameClient', '=', '1234567890')->get();
        foreach (Client::all() as $Vente) {
            $items = Payment::where('nameClient', '=', '1234567890')->get();
            $items->push($Vente);

            //calcule credit
            //********************************** */
            $_totalLeftAmount = 0;
            foreach (Vente::where('slugClient', '=', $Vente->slugClient)
                ->get() as $_vente) {

                $_totalLeftAmount = $_vente->leftAmount + $_totalLeftAmount;
            }
            $_client = Client::where('slugClient', '=', $Vente->slugClient)->first();
            $_client->credit = $_totalLeftAmount;
            $_client->save();
            /********************************* */


            foreach (Payment::where('slugClient', '=', $Vente->slugClient)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["payment" => $items]);
        }
        return $_section;
    }
    public function allPaymentsClientsPeriod(string $dateDebut, string $dateFin)
    {
        $_section = Vente::where('nameClient', '=', '1234567890')->get();
        foreach (Vente::groupBy('nameClient')->select('nameClient')->get() as $Vente) {
            $items = Payment::where('nameClient', '=', '1234567890')->get();
            $items->push($Vente);
            foreach (Payment::where('nameClient', '=', $Vente->nameClient)
                ->where('datePayment', '>=', $dateDebut)->where('datePayment', '<=', $dateFin)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["payment" => $items]);
        }
        return $_section;
    }
    public function getAllPaymentsFilter(Request $request)
    {
        $_section = Payment::where('id', '=', '0')->get();


        if ($request) {
            // du au type
            if (
                $request->du != '' & $request->au != '' & $request->type != ''
                & $request->nameClient == '' & $request->paymentMethod == ''
            ) {

                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('datePayment', '>=', $request->du)
                    ->where('datePayment', '<=', $request->au)->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $client = Client::where('slugClient', '=', $payment->slugClient)->first();

                    if ($client->typePurchase == strval($request->type)) {
                        $_payments->push($payment);
                    }
                }

                return $_payments;
            }

            // du au type methode
            if (
                $request->du != '' & $request->au != '' & $request->type != ''
                & $request->nameClient == '' & $request->paymentMethod != ''
            ) {

                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('datePayment', '>=', $request->du)
                    ->where('datePayment', '<=', $request->au)
                    ->where('paymentMethod', '=', $request->paymentMethod)
                    ->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $client = Client::where('slugClient', '=', $payment->slugClient)->first();

                    if ($client->typePurchase == strval($request->type)) {
                        $_payments->push($payment);
                    }
                }

                return $_payments;
            }

            /***********************************************************************************/
            // du au
            if (
                $request->du != '' & $request->au != '' & $request->type == '' & $request->nameClient == ''
                & $request->paymentMethod == ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('datePayment', '>=', $request->du)
                    ->where('datePayment', '<=', $request->au)->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }
                return $_payments;
            }
            /*********************************************************************************** */
            // type
            if (
                $request->type != '' & $request->du == '' & $request->au == ''
                & $request->nameClient == '' & $request->paymentMethod == ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::orderByRaw('created_at DESC')->get() as $payment) {

                    $client = Client::where('slugClient', '=', $payment->slugClient)->first();

                    if ($client->typePurchase == strval($request->type)) {
                        $_payments->push($payment);
                    }
                }


                return $_payments;
            }
            // type methode
            if (
                $request->type != '' & $request->du == '' & $request->au == ''
                & $request->nameClient == '' & $request->paymentMethod != ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('paymentMethod', '=', $request->paymentMethod)
                    ->orderByRaw('created_at DESC')->get() as $payment) {

                    $client = Client::where('slugClient', '=', $payment->slugClient)->first();

                    if ($client->typePurchase == strval($request->type)) {
                        $_payments->push($payment);
                    }
                }


                return $_payments;
            }

            // nameClient
            if (
                $request->nameClient != '' & $request->du == '' & $request->au == ''
                & $request->type == '' & $request->paymentMethod == ''
            ) {

                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('nameClient', '=', $request->nameClient)->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }

                return $_payments;
            }

            // nameClient methode
            if (
                $request->nameClient != '' & $request->du == '' & $request->au == ''
                & $request->type == '' & $request->paymentMethod != ''
            ) {

                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('nameClient', '=', $request->nameClient)
                ->where('paymentMethod', '=', $request->paymentMethod)
                ->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }

                return $_payments;
            }

            // du au nameClient
            if (
                $request->nameClient != '' & $request->du != '' & $request->au != ''
                & $request->type == '' & $request->paymentMethod == ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('nameClient', '=', $request->nameClient)
                    ->where('datePayment', '>=', $request->du)
                    ->where('datePayment', '<=', $request->au)
                    ->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }

                return $_payments;
            }

            // du au nameClient methode 
            if (
                $request->nameClient != '' & $request->du != '' & $request->au != ''
                & $request->type == '' & $request->paymentMethod != ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('nameClient', '=', $request->nameClient)
                    ->where('datePayment', '>=', $request->du)
                    ->where('datePayment', '<=', $request->au)
                    ->where('paymentMethod', '=', $request->paymentMethod)
                    ->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }

                return $_payments;
            }
            // methode 
            if (
                $request->nameClient == '' & $request->du == '' & $request->au == ''
                & $request->type == '' & $request->paymentMethod != ''
            ) {
                $_payments = Payment::where('id', '=', '0')->get();
                foreach (Payment::where('paymentMethod', '=', $request->paymentMethod)
                    ->orderByRaw('created_at DESC')
                    ->get() as $payment) {

                    $_payments->push($payment);
                }

                return $_payments;
            }

            //cheque
            if ($request->chequeNumber != '') {
                // $_payments = Payment::where('id', '=', '0')->get();
                // foreach (Payment::where('chequeNumber', '=', $request->chequeNumber)
                //     ->where('paymentMethod', '=', 'Chèque')
                //     ->orderByRaw('created_at DESC')
                //     ->get() as $payment) {
                //     $_payments->push($payment);
                // }

                return Payment::where('chequeNumber', '=', $request->chequeNumber)
                    ->where('paymentMethod', '=', 'Chèque')
                    ->get();
            }
        } else {
            return null;
        }

        // return $_section;

        // return Vente::all();
    }
}
