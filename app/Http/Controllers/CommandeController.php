<?php

namespace App\Http\Controllers;

use App\Mail\SupplierMail;
use App\Models\Commande;
use App\Models\DetailCommande;
use App\Models\ItemSection;
use App\Models\Society;
use App\Models\Supplier;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;


class CommandeController extends Controller
{
    public function getAllCommandesDetails()
    {
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        foreach (Commande::all() as $commande) {
            $items = ItemSection::where('fullName', '=', '1234567890')->get();
            $supplier = Supplier::where('slug', '=', $commande->slugSupplier)->first();
            $items->push($commande);
            $items->push($supplier);
            foreach (DetailCommande::where('idCommande', '=', $commande->id)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["commande" => $items]);
        }
        return $_section;
    }

    public function store(Request $request)
    {
        // select supplier 
        $supplier = Supplier::where('slug', '=', Str::slug($request->nameSupplier, '-'))->first();

        $lastcommande = Commande::latest()->first();

        if (!$lastcommande) {
            $reference = 'BL000000';
        } else {
            $reference = $lastcommande->reference;
        }
        $reference++;

        $commande = new Commande();
        // $commande->idSupplier  = 1;
        $commande->nameSupplier  = $request->nameSupplier;

        $commande->remark  = $request->remark;
        $commande->addressLaivraison  = $request->addressLaivraison;

        $commande->reference  = $reference;
        $commande->slugSupplier  = Str::slug($request->nameSupplier, '-');
 
        $commande->save();

        $detailCommande = DetailCommande::where('idCommande', '=', 0)->get();

        foreach (DetailCommande::where('idCommande', '=', null)->get() as $req) {


            $req->idCommande = $commande->id;
            $req->save();
            $detailCommande->push($req);
        }


        // send email
        $title = "[Commande] N" . $commande->reference;
        $supplier_details = Supplier::where('slug', '=', Str::slug($request->nameSupplier, '-'))->first();
        $society = Society::first();

        Mail::to($supplier_details->email, "OUCHAYANE Omar")->send(new SupplierMail(
            $title,
            $supplier_details,
            $detailCommande,
            $commande,
            $society
        ));

        return [$commande, $detailCommande, $supplier_details];
    }
}
