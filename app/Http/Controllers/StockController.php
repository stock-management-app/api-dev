<?php

namespace App\Http\Controllers;

use App\Models\DetailStock;
use App\Models\ItemSection;
use App\Models\Stock;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class StockController extends Controller
{
    public function index()
    {
        // return Stock::all();
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        foreach (Stock::all() as $purchase) {
            $items = ItemSection::where('fullName', '=', '1234567890')->get();
            $items->push($purchase);
            foreach (DetailStock::where('idStock', '=', $purchase->id)->get() as $item) {
                $items->push($item);
            }
            $_section->push(["purchase" => $items]);
        }
        return $_section;
    }
    public function create()
    {
        return view('stocks.create');
    }
    public function store(Request $request)
    {
        $stock = new Stock();
        $stock->billNumber  = $request->billNumber;
        $stock->fullNameUser  = $request->fullNameUser;
        $stock->idSupplier  = 1;
        $stock->nameSupplier  = $request->nameSupplier;
        $stock->slugSupplier  = Str::slug($request->nameSupplier, '-');
        $stock->dateAchat  = $request->dateAchat;
        if ($request->etatPayment == 'Payé') {
            $stock->etatPayment  = $request->etatPayment;
            $stock->payrollAmount  = $request->payrollAmount;

            $stock->paymentMethod  = $request->paymentMethod;
        }
        $stock->save();

        $detailStocks = DetailStock::where('idStock', '=', null)->get();
        $totalPrice = 0;
        foreach ($detailStocks as $req) {
            $totalPrice = $totalPrice + $req->totalPrice;
            $req->idStock = $stock->id;
            $req->save();

            $item = ItemSection::where('slug', '=', $req->slugItem)->get()[0];
            // $item->shortDescription = $req->quantity;
            $item->quantity = $item->quantity + $req->quantity;
            if ($req->grandComptePrice != '')
                $item->grandComptePrice = $req->grandComptePrice;
            if ($req->specialPrice != '')
                $item->specialPrice = $req->specialPrice;
            if ($req->particularPrice != '')
                $item->particularPrice = $req->particularPrice;
            if ($req->professionalPrice != '')
                $item->professionalPrice = $req->professionalPrice;
            $item->save();
        }
        $stock->totalPrice  = $totalPrice;
        $stock->leftAmount  = $stock->totalPrice - $request->payrollAmount;

        $stock->save();

        return DetailStock::where('idStock', '=', null)->get();
    }


    public function show($id)
    {
        return Stock::findOrFail($id);
    }
    public function edit(Stock $stock)
    {
    }

    public function update($id, Request $request)
    {
        // echo "<script>console.log('request'.$request)</script>";
        // echo "<script>console.log('stock')</script>";

        $stock = Stock::find($id);
        // $request->offre='true';
        $stock->dateStartOffre = $request->dateStartOffre;
        $stock->dateExpireOffre = $request->dateExpireOffre;
        $stock->remise = $request->remise;
        $stock->offre = $request->offre;
        $stock->save();

        return Stock::all();
    }
    public function destroy(Stock $stock)
    {
    }

    // API
    public function all()
    {
        return Stock::all();
    }
    public function stocksSection($id)
    {
        return Stock::where('idSection', '=', $id)->get();
    }
    public function getAllPurchasesFilter(Request $request)
    {
        $_section = ItemSection::where('fullName', '=', '1234567890')->get();
        $type = "";
        if ($request) {
            if ($request->du != '' & $request->au != '' & $request->nameSupplier != '') {
                foreach (Stock::where('dateAchat', '>=', $request->du)
                    ->where('dateAchat', '<=', $request->au)
                    ->where('slugSupplier', '<=', Str::slug($request->nameSupplier, '-'))
                    ->get() as $purchase) {

                    $items = DetailStock::where('id', '=', '0')->get();
                    $items->push($purchase);
                    foreach (DetailStock::where('idStock', '=', $purchase->id)->get() as $item) {
                        $items->push($item);
                    }
                    $_section->push(["purchase" => $items]);
                }
                return $_section;
            }
            if ($request->du != '' & $request->au != '' & $request->nameSupplier == "") {
                foreach (Stock::where('dateAchat', '>=', $request->du)
                    ->where('dateAchat', '<=', $request->au)
                    ->get() as $purchase) {

                    $items = DetailStock::where('id', '=', '0')->get();
                    $items->push($purchase);
                    foreach (DetailStock::where('idStock', '=', $purchase->id)->get() as $item) {
                        $items->push($item);
                    }
                    $_section->push(["purchase" => $items]);
                }
                return $_section;
            }
            if ($request->du == "" & $request->au == "" & $request->nameSupplier != "") {
                foreach (Stock::where('slugSupplier', '=', Str::slug($request->nameSupplier, '-'))
                    ->get() as $purchase) {

                    $items = DetailStock::where('id', '=', '0')->get();
                    $items->push($purchase);
                    foreach (DetailStock::where('idStock', '=', $purchase->id)->get() as $item) {
                        $items->push($item);
                    }
                    $_section->push(["purchase" => $items]);
                }
                return $_section;
            }
            // if ($request->nameClient != '' & $request->du == '' & $request->au == '' & $request->type == '') {
            //     foreach (Vente::where('nameClient', '=', $request->nameClient)->get() as $Vente) {

            //         $items = ItemSection::where('fullName', '=', '1234567890')->get();
            //         $items->push($Vente);
            //         foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
            //             $items->push($item);
            //         }
            //         $_section->push(["vente" => $items]);
            //     }
            //     return $_section;
            // }
            // if ($request->nameClient != '' & $request->du != '' & $request->au != '' & $request->type == '') {
            //     foreach (Vente::where('nameClient', '=', $request->nameClient)
            //         ->where('dateVente', '>=', $request->du)
            //         ->where('dateVente', '<=', $request->au)->get() as $Vente) {

            //         $items = ItemSection::where('fullName', '=', '1234567890')->get();
            //         $items->push($Vente);
            //         foreach (DetailVente::where('idCommande', '=', $Vente->id)->get() as $item) {
            //             $items->push($item);
            //         }
            //         $_section->push(["vente" => $items]);
            //     }
            //     return $_section;
            // }
        } else {
            return null;
        }

        // return $_section;

        // return Vente::all();
    }
}
