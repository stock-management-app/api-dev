<?php

namespace App\Http\Controllers;

use App\Models\DetailCommande;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class DetailCommandeController extends Controller
{
    public function store(Request $request)
    {
        $detailCommande = new DetailCommande();
        $detailCommande->nameItem = $request->nameItem.' - '.$request->typeEmbalage;
        $detailCommande->quantity = $request->quantity;
        $detailCommande->remark = $request->remark;
        $detailCommande->slugItem = Str::slug($request->nameItem, '-');
        // $detailCommande->idUser = $request->idUser;
 
        $detailCommande->save();

        return DetailCommande::where('idCommande', '=', null)->get();
    }
}
