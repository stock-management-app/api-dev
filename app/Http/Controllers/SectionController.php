<?php

namespace App\Http\Controllers;

use App\Models\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    public function index(){
        return Section::all();
    }
    public function all(){
        return Section::all();
    }
    public function create()
    {
        return view('sections.create');
    }
    public function store(Request $request)
    {
        $section = new Section();

        $section->fullName = $request->fullName;
        $section->description = $request->description;

        $section->save();

        // $sections = Section::all();
        return Section::all();

        // echo "<script> alert('')</script>";
    }
    public function show(Section $section)
    {

    }
    public function edit(Section $section)
    {

    }
    public function update(Request $request, Section $section)
    {

    }
    public function destroy(Section $section)
    {

    }
}
