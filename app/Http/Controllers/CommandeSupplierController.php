<?php

namespace App\Http\Controllers;

use App\Mail\CommandeSupplierMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommandeSupplierController extends Controller
{
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        $title = '[Confirmation] Thank you for your order';
        $customer_details = [
            'name' => 'Arogya',
            'address' => 'kathmandu Nepal',
            'phone' => '123123123',
            'email' => 'nom18022021@gmail.com'
        ];
        $order_details = [
            'SKU' => 'D-123456',
            'price' => '10000',
            'order_date' => '2020-01-22',
        ];

        $sendmail = Mail::to($customer_details['email'])->send(new CommandeSupplierMail($title, $customer_details, $order_details));
        if (empty($sendmail)) {
            return response()->json(['message' => 'Mail Sent Sucssfully'], 200);
        }else{
            return response()->json(['message' => 'Mail Sent fail'], 400);
        }
    }
    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
}
