<?php

namespace App\Http\Controllers;


use App\Models\Client;
use App\Models\Vente;
use App\Models\DetailVente;
use App\Models\ItemSection;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class DetailVenteController extends Controller
{
    public function index()
    {
        return DetailVente::where('idCommande', '=', null)->get();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $detailVente = new DetailVente();
        $detailVente->nameItem = $request->nameItem;
        $detailVente->quantity = $request->quantity;
        $detailVente->fullNameUser  = $request->fullNameUser;
        
        $detailVente->slugItem = Str::slug($request->nameItem, '-');
        // $detailVente->idUser = $request->idUser;

        $detailVente->save();

        return DetailVente::where('idCommande', '=', null)->get();
    }

    public function show($id)
    {
        $detailVentes = DetailVente::where('idCommande', '=', $id)->get();
        $Vente = Vente::where('id', '=', $id)->select("*")->first();
        $client = Client::where('slugClient', '=', $Vente->slugClient)->first();
        return [$detailVentes, $Vente, $client];
        // return $Vente->slugClient;
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $detailVente = DetailVente::where('id', '=', $id)->first();
        $detailVente->delete();
        return DetailVente::where('idCommande', '=', null)->get();
    }
    public function postDetailVenteUpdate(Request $request)
    {
        $priceHT = 0;
        $priceTTC = 0;
        $totalPriceHT = 0;
        $totalPriceTTC = 0;
        $tva = 0;
        $detailVente = new DetailVente();
        $detailVente->nameItem = $request->nameItem;
        $detailVente->quantity = $request->quantity;
        $detailVente->slugItem = Str::slug($request->nameItem, '-');
        $detailVente->idCommande = $request->idCommande;

        $client = Client::where('slugClient', '=', Str::slug($request->nameClient, '-'))->first()   ;
        $item = ItemSection::where('slug', '=', Str::slug($request->nameItem, '-'))->first();
        $tva = $item->tva;
        if ($client->typePurchase == "Grand Compte") {
            $priceHT = $item->grandComptePrice;
        }
        if ($client->typePurchase == "Spéciale") {
            $priceHT = $item->specialPrice;
        }
        if ($client->typePurchase == "Particulier") {
            $priceHT = $item->particularPrice;
        }
        if ($client->typePurchase == "Professionnel") {
            $priceHT = $item->professionalPrice;
        }
        $totalPriceHT = $priceHT * $request->quantity;
        $priceTTC = $priceHT + ($priceHT / 100) * $tva;
        $totalPriceTTC = $priceTTC * $request->quantity;
        $totalTVA = ($priceHT / 100) * $tva * $request->quantity;

        $detailVente->priceHT = $priceHT;
        $detailVente->totalPriceHT = $totalPriceHT;
        $detailVente->totalPriceTTC  = $totalPriceTTC ;
        $detailVente->totalTVA  = $totalTVA ;
        $detailVente->tva  = $tva ;
        $detailVente->priceTTC  = $priceTTC ;
        $detailVente->save();

        return DetailVente::where('idCommande', '=', $request->idCommande)->get();
    }
}
